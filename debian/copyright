Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: bemenu
Upstream-Contact: Jari Vetoniemi <mailroxas@gmail.com>
Source: https://github.com/Cloudef/bemenu
Files-Excluded: doxygen/doxygen-qmi-style/*
Comment:
 The upstream source includes a copy of the "Qt Minimal" theme by Sergey Kozlov
 which has been excluded since the origin repository does not contain a license.
 .
 https://github.com/skozlovf/doxygen-qmi-style

Files: *
Copyright: 2014-2019, Jari Vetoniemi <mailroxas@gmail.com>
License: GPL-3+

Files: lib/*
Copyright: 2014-2019, Jari Vetoniemi <mailroxas@gmail.com>
License: LGPL-3+

Files: lib/renderers/wayland/wlr-layer-shell-unstable-v1.xml
       lib/renderers/wayland/xdg-output-unstable-v1.xml
Copyright: 2017, Drew DeVault
           2017, Red Hat Inc
License: MIT-like

Files: lib/renderers/x11/xkb_unicode.c
Copyright: 2006-2017, Camilla Löwy <elmindreda@glfw.org>
           2002-2006, Marcus Geelnard
           2001, Markus G. Kuhn <mkuhn@acm.org>
License: Zlib

Files: debian/*
Copyright: 2020, Peter Colberg <peter@colberg.org>
           2022, Tzafrir Cohen <tzafrir@debian.org>
License: GPL-3+

License: GPL-3+
 On Debian systems the full text of the GPL-3 can be found in
 /usr/share/common-licenses/GPL-3

License: LGPL-3+
 On Debian systems the full text of the LGPL-3 can be found in
 /usr/share/common-licenses/LGPL-3

License: MIT-like
 Permission to use, copy, modify, distribute, and sell this software and its
 documentation for any purpose is hereby granted without fee, provided that the
 above copyright notice appear in all copies and that both that copyright notice
 and this permission notice appear in supporting documentation, and that the
 name of the copyright holders not be used in advertising or publicity
 pertaining to distribution of the software without specific, written prior
 permission. The copyright holders make no representations about the suitability
 of this software for any purpose.  It is provided "as is" without express or
 implied warranty.
 .
 THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT
 SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
 ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 SOFTWARE.

License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would
    be appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and must not
    be misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source
    distribution.
